package com.teamsakhya.googleconsumer;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.springframework.beans.PropertyEditorRegistrar;



public class Consumer implements MessageListener{
	private static final String QUEUE_NAME = "rahul";
	private static final String URL = "tcp://localhost:61616";


	public static void main(String[] args) throws JMSException {
		Logger logger = Logger.getLogger(Consumer.class);

		Layout layout = new SimpleLayout();
		Appender appender = new ConsoleAppender(layout);
		logger.addAppender(appender);

		ConnectionFactory factory = new ActiveMQConnectionFactory(URL);
		logger.info("creating coonection factory object");

		Connection connection = factory.createConnection();
		connection.start();
		logger.info("creating connectiom via factory");

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		logger.info("creating session via connection");

		Destination destination = session.createQueue(QUEUE_NAME);
		logger.info("creating queue via session");

		MessageConsumer consumer = session.createConsumer(destination);
		consumer.setMessageListener(new Consumer());
		logger.info("creating messgaeconsumer via session");

		Message message=consumer.receive();

     
		if (((TextMessage)message).getText().contains("Longname")) {
			logger.info(((TextMessage)message).getText());
			
		}else {
			logger.fatal(((TextMessage)message).getText());
		}
		System.out.println(((TextMessage)message).getText());
		
	}

	public void onMessage(Message arg0) {
		// TODO Auto-generated method stub
		
	}

}
